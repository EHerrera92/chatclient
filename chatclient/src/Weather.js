import {useEffect, useState} from "react";

export function Weather() {
    let [imgIcon, setImgIcon] = useState("");
    let [city, setCity] = useState("");
    let [temp, setTemp] = useState(0);
    let [weather, setWeather] = useState("");
    let [isLoaded, setLoaded] = useState(false);

    useEffect(() => {
        navigator.geolocation.getCurrentPosition((loc) => {
            console.log(loc);
            var url = 'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={key}';
            url = url.replace('{lat}', loc.coords.latitude);
            url = url.replace('{lon}', loc.coords.longitude);
            url = url.replace('{key}', '47ce48ce8fab212402a48150e40b31f3');
            fetch(url, {method: 'GET'})
            .then(response => response.json())
            .then(json => {
                setWeather(json.weather[0].main);
                setCity(json.name);
                setTemp(json.main.temp);
                setImgIcon('http://openweathermap.org/img/wn/' + json.weather[0].icon + '@2x.png');
                setLoaded(true);
            });
        }, (err) => {
            alert(err);
        });
    }, []);


    if(!isLoaded)
        return <></>;
    return <div style={{paddingLeft: '20px'}}>
        <img src={imgIcon}/><br/>
        The weather in {city} is {weather} with a temperature of {Math.round(temp)} degrees.
    </div>;
}