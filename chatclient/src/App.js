import logo from './logo.svg';
import './App.css';
import {useState, useEffect } from 'react';
import {Weather} from './Weather';

var _token = null;
var _ws = null;

function App() {
  useEffect(() => {
    //init
  }, []);

  let [loggedIn, setLoggedIn] = useState(false);
  let [username, setUsername] = useState("");
  let [password, setPassword] = useState("");
  let [loginErrorMsg, setLoginErrorMsg] = useState(null);
  let [msg, setMsg] = useState("");
  let [msgs, setMsgs] = useState([]);

  const tryLogin = async function() {
    var response = await fetch('http://192.168.1.23/auth', {
      method: 'POST',
      cache: 'no-cache',
      headers: {'content-type': 'application/json'},
      body: JSON.stringify({username: username, password: password})
    });
    if(response.status == 401) {
      setLoginErrorMsg('Wrong username or password.');
    }
    else if(response.status == 200) {
      let jsonResponse = await response.json();
      console.log(`Logged in successfully and received token ${jsonResponse.token}.`);
      _token = jsonResponse.token;
      _ws = new WebSocket(`ws://192.168.1.23:80?token=${_token}`);
      _ws.onmessage = (event) => {
        var cmd = JSON.parse(event.data);
        if(cmd.type == 'connected'){
          console.log(`User ${cmd.data[0]} has entered the channel.`);
          msgs.push({name: 'Server', msg: `${cmd.data[0]} has entered the channel.`, bubbleType: 'ChatBubble-Server'});
        }
        else if(cmd.type == 'disconnected'){
          console.log(`User ${cmd.data[0]} has left the channel.`);
          msgs.push({name: 'Server', msg: `${cmd.data[0]} has left the channel.`, bubbleType: 'ChatBubble-Server'});
        }
        else if(cmd.type == 'receivemessage'){
          console.log(`User ${cmd.data[0]} sent message ${cmd.data[1]}`);
          msgs.push({name: cmd.data[0], msg: cmd.data[1], bubbleType: cmd.data[0] == 'Emmanuel' ? 'ChatBubble-Boy' : 'ChatBubble-Girl'});
        }
        console.log(msgs);  
        setMsgs(msgs.filter(() => true));
        document.getElementById('ChatLogElem').scrollTop = document.getElementById('ChatLogElem').scrollHeight;
        document.getElementById('ding').play();
      };
      setLoginErrorMsg(null);
      setLoggedIn(true);
    }
    else {
      setLoginErrorMsg('An error occured while logging you in. Try again later.');
    }
  };
  const sendMsg = function() {
    console.log(msg);
    console.log(msg == "goodbye");
    if(msg == "goodbye") {
      console.log('in here');
      _ws.send(JSON.stringify({
        token: _token,
        type: 'disconnected'
      }));
    }
    else {
      _ws.send(JSON.stringify({
        token: _token,
        type: 'sendmessage',
        data: [msg]
      }));
      setMsg("");
    }
  };
  if(!loggedIn)
    return(
      <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', minHeight: '100vh'}}>
        <div style={{maxWidth: '400px'}}>
          <h3>Welcome to MyChat v1.0.0a</h3>
          Enter your username and password to enter the chat room.
          <input type="text" placeholder="Username" onChange = {(e) => setUsername(e.target.value)} style={{width: '100%', height: '28px', marginBottom:'8px', marginTop: '8px'}}/>
          <input type="password" placeholder="Password" onChange = {(e) => setPassword(e.target.value)} style={{width: '100%', height: '28px', marginBottom:'8px'}}/>
          <input type="button" value="Login" style={{width: '100%', height: '28px' }} onClick={tryLogin}/><br/><br/>
          <div style={{display: loginErrorMsg ? 'flex' : 'none', justifyContent: 'center', alignItems: 'center', height: '100%', backgroundColor: 'pink', color: 'black', borderStyle: 'solid', borderColor: 'black', borderWidth: '2px', borderRadius: '3px', paddingBottom: '4px', textAlign: 'center', verticalAlign: 'middle'}}>
            {loginErrorMsg}
          </div>
        </div>
      </div>
    );
  return (
    <>
    <audio id="ding">
      <source src="http://192.168.1.23/ding.wav" type="audio/wav"></source>
    </audio>
    <h3>&nbsp;&nbsp;&nbsp;Welcome to Coolchat.com :)</h3>
    <Weather style={{marginLeft: '20px'}}/>
    <div className="Container">
      <div className="ChatLog" id="ChatLogElem">
        {msgs.map(m => <div className={m.bubbleType}>{m.name}: {m.msg}</div>)}
      </div>
      <input value={msg} className="ChatNewMessage" onChange={(e) => setMsg(e.target.value)} placeholder="Enter a message"></input>
      <div className="ChatSendMessage" onClick={sendMsg}>Send</div>
    </div>
    </>
  );
}



export default App;
